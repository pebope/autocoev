# AutoCoEv -- A High-Throughput In Silico Pipeline for Predicting Inter-Protein Coevolution

_Petar B. Petrov<sup>1,2*</sup>, Luqman O. Awoniyi<sup>1,2</sup>, Vid Šuštar1<sup>1</sup>, M. Özge Balci<sup>1,2</sup> and Pieta K. Mattila<sup>1,2</sup>_

1. MediCity Research Laboratories, Institute of Biomedicine, University of Turku, 20014 Turku, Finland
2. Turku Bioscience Centre, University of Turku and Åbo Akademi University, 20520 Turku, Finland

>**Contact:** Petar Petrov (*current* email: petar.petrov ат oulu.fi), University of Oulu.

Paper published in [International Journal of Molecular Sciences; March 2022, 23(6), 3351](https://www.mdpi.com/1422-0067/23/6/3351).

## Abstract
Protein–protein interactions govern cellular processes via complex regulatory networks, which are still far from being understood. Thus, identifying and understanding connections between proteins can significantly facilitate our comprehension of the mechanistic principles of protein functions. Coevolution between proteins is a sign of functional communication and, as such, provides a powerful approach to search for novel direct or indirect molecular partners. However, an evolutionary analysis of large arrays of proteins in silico is a highly time-consuming effort that has limited the usage of this method for protein pairs or small protein groups. Here, we developed AutoCoEv, a user-friendly, open source, computational pipeline for the search of coevolution between a large number of proteins. By driving 15 individual programs, culminating in CAPS2 as the software for detecting coevolution, AutoCoEv achieves a seamless automation and parallelization of the workflow. Importantly, we provide a patch to the CAPS2 source code to strengthen its statistical output, allowing for multiple comparison corrections and an enhanced analysis of the results. We apply the pipeline to inspect coevolution among 324 proteins identified to be located at the vicinity of the lipid rafts of B lymphocytes. We successfully detected multiple coevolutionary relations between the proteins, predicting many novel partners and previously unidentified clusters of functionally related molecules. We conclude that AutoCoEv, can be used to predict functional interactions from large datasets in a time- and cost-efficient manner.

## Pipeline Overview
![img](doc/ijms-23-03351-g001.png)

**Figure 1. The distinct steps of the AutoCoEv pipeline are outlined below:**
1) 🟡 **Yellow**: Reading the user-provided lists of proteins of interest and species to be searched, the script communicates between databases to extract genes (ODB), orthologous groups (OG), and protein identifiers (ID). Homologous sequences are then blasted against the UniProt sequences from the reference organism (e.g., mouse or human) in order to prepare a FASTA list of most appropriate orthologues. Before MSA, orthologues are assessed by Guidance and too-divergent ones are removed.
2) 🟠 **Orange**: Orthologues are aligned by selected method (MAFFT, MUSCLE, or PRANK) and scanned by Gblocks to report regions of low quality. PhyML calculates trees from the MSA generated in the previous step, optionally using an external tree as a guide.
3) 🟢 **Green**: Creates all unique protein pairs in folders, with each folder having two sub-folders for MSA and (if prepared by PhyML) trees.
4) 🔵 **Blue**: CAPS2 is run for each protein pair folder in a parallelized fashion via GNU/Parallel. If coevolution is detected, CAPS2 is run again, this time “reversing” the protein load order (e.g., A vs. B followed by B vs. A).
5) 🟣 **Purple**: The output in each pairs folder is inspected and processed, followed by FDR correction of p-values and chi-squared test. 

## Requirements
AutoCoEv takes advantage of the following programs (the ones available from Debian/Ubuntu's repositories are indicated by a checkmark):
- [ ] [CAPS2](https://caps.tcd.ie/caps/home.html) ([patched](./sources/patches/) by us): Coevolution Analysis using Protein Sequences
- [x] [Datamash](https://www.gnu.org/software/datamash/): Basic numeric, textual and statistical operations
- [x] [Exonerate](https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate): A generic tool for sequence alignment
- [ ] [Gblocks](http://molevol.cmima.csic.es/castresana/Gblocks.html): Select blocks of evolutionarily conserved sites	
- [ ] [Guidance](http://guidance.tau.ac.il/): GUIDe tree based AligNment ConfidencE	
- [x] [MAFFT](https://mafft.cbrc.jp/alignment/software/): A multiple alignment program
- [x] [MUSCLE3](https://www.drive5.com/muscle/): MUltiple Sequence Comparison by Log-Expectation
- [x] [NCBI BLAST+](https://blast.ncbi.nlm.nih.gov/Blast.cgi): BLAST+ Command Line Applications	
- [x] [PRANK](http://wasabiapp.org/software/prank/): A probabilistic multiple alignment program
- [x] [Parallel](http://www.gnu.org/software/parallel/): A shell tool for executing jobs in parallel
- [x] [PhyML](https://github.com/stephaneguindon/phyml/): Phylogenetic estimation using Maximum Likelihood
- [x] [R](https://www.r-project.org/): Software environment for statistical computing and graphics
- [ ] [SeqKit](https://bioinf.shenwei.me/seqkit/): Atoolkit for FASTA/Q file manipulation
- [x] [squizz](ftp://ftp.pasteur.fr/pub/gensoft/projects/squizz/): A sequence/alignment format checker and converter
- [ ] [TreeBeST](https://github.com/Ensembl/treebest) (Ensemble mod): Tree Building guidEd by Species Tree	

## Setting up on Debian/Ubuntu
### Install packages available from the repositories
```sh
# These are directly used by AutoCoEv:
sudo apt install muscle mafft datamash ncbi-blast+ exonerate prank squizz parallel phyml r-base

# Needed by Guidance:
sudo apt install bioperl
sudo apt install libbio-coordinate-perl libbio-db-ace-perl libbio-eutilities-perl libbio-procedural-perl libbio-samtools-perl

# Needed by TreeBeST:
sudo apt install flex bison
```
### Install CAPS2
AutoCoEv uses a patched vesrion of CAPS2 that has improved verbosity of the results and other enhancements. The easiest approach is to get the static binary provided by us, [called vCAPS-static-sort](sources/caps-static/vCAPS-static-sort), found in the [sources/caps-static](sources/caps-static) folder.

```sh
# Copy system-wide and make sure it is executable
sudo cp -a vCAPS-static-sort /usr/local/bin
chmod +x /usr/local/bin vCAPS-static-sort
```
If you want to build it from source yourself, check [sources/caps-dynamic](sources/caps-dynamic/).

### Install Gblocks
```sh
# Get the source (alternatively check the "sources/" folder of AutoCoEv) and extract
wget http://molevol.cmima.csic.es/castresana/Gblocks/Gblocks_Linux64_0.91b.tar.Z
tar xvf Gblocks_Linux64_0.91b.tar.Z

# Copy system-wide and make sure it is executable
sudo cp Gblocks_0.91b/Gblocks /usr/local/bin
sudo chmod +x /usr/local/bin/Gblocks
```

### Build and install Guidance
```sh
# Get the source (alternatively check the "sources/" folder of AutoCoEv) and extract
wget http://guidance.tau.ac.il/guidance.v2.02.tar.gz
tar xvf guidance.v2.02.tar.gz

# Build, copy system-wide and make sure it is executable
cd guidance.v2.02
make
cd ..
sudo cp -a guidance.v2.02 /usr/lib/guidance
sudo chmod +x /usr/lib/guidance/www/Guidance/guidance.pl
```

### Install SeqKit
```sh
# Get the source and extract
wget https://github.com/shenwei356/seqkit/releases/download/v2.7.0/seqkit_linux_amd64.tar.gz
tar xvf seqkit_linux_amd64.tar.gz

# Copy system-wide and make sure it is executable
sudo cp seqkit /usr/local/bin
sudo chmod +x /usr/local/bin/seqkit
```

### Build and install TreeBeST
```sh
# Get the source and extract
wget https://github.com/Ensembl/treebest/archive/347fa82/treebest-347fa82a0ce1c169849053fdc9ff7d19d221f290.tar.gz
tar xvf treebest-347fa82a0ce1c169849053fdc9ff7d19d221f290.tar.gz
cd treebest-347fa82a0ce1c169849053fdc9ff7d19d221f290

# Build (the C flag is needed for newer GCC), copy system-wide and make sure it is executable
sed -i "/^CFLAGS/s/=/+=/" Makefile
CFLAGS="$CFLAGS -fcommon" make
sudo cp treebest /usr/local/bin
sudo chmod +x /usr/local/bin/treebest
```

### PhyML and MAFFT symlinks
AutoCoEv uses phyml executable directly, not through the wrapper provided by Debian/Ubuntu. Therefore, make a symlink in `/usr/local/bin/`. Also, AutoCoEv expects to find MAFFT aliases in your `$PATH`. Either add `/usr/lib/mafft/bin` to your `$PATH` or create symlinks.
```sh
cd /usr/local/bin/

# PhyML executable symlink
sudo ln -s /usr/lib/phyml/bin/phyml .

# MAFFT aliases symlinks
sudo ln -s /usr/lib/mafft/mafft-linsi .
sudo ln -s /usr/lib/mafft/mafft-einsi .
sudo ln -s /usr/lib/mafft/mafft-ginsi .
sudo ln -s /usr/lib/mafft/mafft-fftns .
sudo ln -s /usr/lib/mafft/mafft-fftnsi .
```
## Setting up on CRUX
I use [CRUX](https://crux.nu/) distribution of GNU/Linux. Setting up AutoCoEv there is quite straightforward.
* the `contrib` ports collection should to be enabled (Point 6.2.5 from the [Handbook](https://crux.nu/Main/Handbook3-7)).
* grab my ports repository HttpUp file ([ppetrov.httpup](https://raw.githubusercontent.com/slackalaxy/crux-ports/main/ppetrov/ppetrov.httpup)) and Public key [(ppetrov.pub](https://raw.githubusercontent.com/slackalaxy/crux-ports/main/ppetrov/ppetrov.pub)) and place them in `/etc/ports`. Then just sync the ports collections and install what's needed:
```sh
ports -u

# Build caps first, because otherwise it will try to link to a newer version of BppSuite, required by PRANK
prt-get depinst caps-coev
prt-get depinst datamash gblocks guidance mafft muscle ncbi-blast+ prank parallel phyml r seqkit  squizz treebest wget
```

## Usage
* **Please, read the [Manual](doc/Manual_AutoCoEv.pdf).** 
* **Get familiar with the [settings.conf](./settings.conf) file.** This is where you set up the the pipeline.

### Input files
Briefly, you need to prepare the following files:
* List of proteins, e.g. [`proteins.tsv`](./proteins/3047.mmus.tsv). Columns indicate UniProt IDs, Names and Subgroup (optional. If not necessary, put the same subgroup for all). The name of the file does not matter, place it in the subfolder `proteins/`. If you have more than 2000 input proteins, then these should be split over several files (e.g. proteins_1-1000.tsv, proteins_1001-2000.tsv. See the Manual for details.) Preview:
```
O09167	Rpl21	Parvulin-associated_pre-rRNP
O43143	DHX15	Parvulin-associated_pre-rRNP
O76021	RSL1D1	Parvulin-associated_pre-rRNP
O95995	GAS8	Parvulin-associated_pre-rRNP
P09405	Ncl	Parvulin-associated_pre-rRNP
P11276	Fn1	Parvulin-associated_pre-rRNP
P12970	Rpl7a	Parvulin-associated_pre-rRNP
P14148	Rpl7	Parvulin-associated_pre-rRNP
P14869	Rplp0	Parvulin-associated_pre-rRNP
...snip...
```
* List of species, e.g. [`placental.tsv`](./placental.tsv). Columns indicate taxid and species names. This file is specified in `settings.conf` by `SPECIES="placental.tsv"`. Preview:
```
9606	Homo_sapiens
9598	Pan_troglodytes
9597	Pan_paniscus
9593	Gorilla_gorilla
9601	Pongo_abelii
61853	Nomascus_leucogenys
60711	Chlorocebus_sabaeus
9541	Macaca_fascicularis
9544	Macaca_mulatta
9555	Papio_anubis
...snip...
```
* Optionally, a phylogenetic tree ([`placental.nwk`](./placental.nwk)) to be used as a guide for PhyML and PRANK can be provided from an alternative source (such as [TimeTree](https://timetree.org/)). The tree file is specified in `settings.conf` by `EXTTREE="placental.nwk"` and should be in Newick format.

**Check list for the proteins and species files:**
- [x] no column headers
- [x] columns are tab separated
- [x] UNIX line endings (LF)
- [x] no duplicates
- [x] no empty rows, exept the last one
- [x] the file should end with a new line
- [x] the number of input proteins should not exceed 2000 per file
- [ ] define different subgroups, e.g. for proteins known to belong to different complexes

### Basic configuration
Briefly, you need to check the following key variables in `settings.conf`:
```sh
# Folder where your proteins.tsv file is; no need to change
PROTEIN="proteins/"

# list of species to be searched for orthologues
SPECIES="placental.tsv"

# Taxid code of the species your proteins belong to. Example below is mouse. Use "9606" for human.
ORGANISM="10090"

# Level at which to search for orthologues. These are NCBI taxonomy nodes (levels) where ortho-
# logous groups (OGs) are calculated at OrthoDB. Examples: 2759 (Eukaryota); 33208 (Metazoa);
# 7742 (Vertebrata); 32523 (Tetrapoda); 40674 (Mammalia).
LEVEL="40674"

# What program to use at the MSA step. Choices are: prank, muscle, as well as different mafft
# aliases, such as: "mafft-linsi", "mafft-ginsi", "mafft-einsi", "mafft-fftns", "mafft-fftnsi".
MSAMETHOD="muscle"
```

### Start
The pipeline is started in terminal by:
```sh
bash ./start.sh
```
This will walk you through the different steps. The menu entries are quite self explanatory.

### Results interpretation
Upon completion, AutoCoEv will produce a file called allResidues.tsv, where table columns are as follows:
| Column      | Description |
| ----------- | ----------- |
|NameA|Name of Protein A|
|idA|UniProt ID of Protein A|
|NameB|Name of Protein B|
|idB|UniProt ID of Protein B|
|colA|Position of co-evolving residue in the MSA of Protein A|
|realA|Position of co-evolving residue in Protein A (reference organism, unaligned)|
|colB|Position of co-evolving residue in the MSA of Protein B|
|realB|Position of co-evolving residue in Protein B (reference organism, unaligned)|
|seqA|Amino acid residue at position (Protein A, unaligned)|
|seqB|Amino acid residue at position (Protein B, unaligned)|
|GblAB|Gblocks score of the MSA columns, mean for colA + colB|
|GapsAB|Gaps score of the MSA columns, mean for colA + colB|
|DivsAB|Diversity score of the MSA columns, mean for colA + colB|
|corrT|Correlation threshold (mean for both "directions" of the CAPS run)|
|corr|Correlation (mean for both "directions" of the CAPS run)|
|normC|Correlation, normalized to threshold (mean for both "directions" of the CAPS run)|
|boot|CAPS2 bootstrap value (mean for both "directions" of the CAPS run)|
|p_value|P-value (mean for both "directions" of the CAPS run)|
|bonferroni|Binferroni corrected p-value|
|holm|Holm adjusted p-value|
|bh|Benjamini & Hochberg adjusted p-value (a.k.a FDR)|
|hochberg|Hochberg adjusted p-value|
|hommel|Hommel adjusted p-value|
|by|Benjamini & Yekutieli adjusted p-value|


### Note
It has come to my attention, that `fastaindex` produces an empty file on Debian and Ubuntu. If you have this problem, drop me an email.

## Citation
Petrov, P.B.; Awoniyi, L.O.; Šuštar, V.; Balci, M.Ö.; Mattila, P.K. **AutoCoEv—A High-Throughput In Silico Pipeline for Predicting Inter-Protein Coevolution.** Int. J. Mol. Sci. 2022, 23, 3351. https://doi.org/10.3390/ijms23063351